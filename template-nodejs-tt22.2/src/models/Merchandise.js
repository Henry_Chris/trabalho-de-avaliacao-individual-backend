const DataTypes = require("sequelize");
const sequelize = require("../config/sequelize");

const Merchandise = sequelize.define('Merchandise', {
    model: {
        type: DataTypes.STRING,
        allowNull: false
    },
    price: {
        type: DataTypes.DOUBLE,
        allowNull: false
    },
    photo: {
        type: DataTypes.TEXT,
    },
    condition: {
        type: DataTypes.STRING,
        allowNull: false
    },
    paymentMethod: {
        type: DataTypes.STRING,
        allowNull: false
    },
    remainingWarranty: {
        type: DataTypes.DATEONLY,
    },

});

Merchandise.associate = function(models) {
    Merchandise.belongsTo(models.Client);
    Merchandise.belongsTo(models.Seller);
};

module.exports = Merchandise;



