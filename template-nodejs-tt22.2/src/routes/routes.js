const {Router} = require('express');
const ClientController = require('../controllers/ClientController');
const MerchandiseController = require('../controllers/MerchandiseController');
const SellerController = require('../controllers/SellerController');
const router = Router();

//routes for Clients
router.post('/client', ClientController.create);
router.get('/client', ClientController.index);
router.get('/client/:id', ClientController.show);
router.put('/client/:id', ClientController.update);
router.delete('/client/:id', ClientController.destroy);

//routes for Sellers
router.post('/seller', SellerController.create);
router.get('/seller', SellerController.index);
router.get('/seller/:id', SellerController.show);
router.put('/seller/:id', SellerController.update);
router.delete('/seller/:id', SellerController.destroy);

//routes for Merchandise
router.post('/merchandise', MerchandiseController.create);
router.get('/merchandise', MerchandiseController.index);
router.get('/merchandise/:id', MerchandiseController.show);
router.put('/merchandise/:id', MerchandiseController.update);
router.delete('/merchandise/:id', MerchandiseController.destroy);
router.put('/merchandise/purchase/:merchandiseId/Client/:ClientId', MerchandiseController.purchase);
router.put('/merchandise/cancelPurchase/:merchandiseId', MerchandiseController.cancelPurchase);
router.put('/merchandise/sell/:merchandiseId/Seller/:SellerId', MerchandiseController.sell);
router.put('/merchandise/cancelSell/:merchandiseId', MerchandiseController.cancelSell);

module.exports = router;