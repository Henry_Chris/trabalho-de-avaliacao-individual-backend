const {response} = require('express');
const Merchandise = require('../models/Merchandise');
const Client = require('../models/Client');
const Seller = require('../models/Seller')

const create = async(req,res) => {
    try{
          const merchandise = await Merchandise.create(req.body);
          console.log(req.body);
          return res.status(201).json({message: "Produto anunciado para venda!", merchandise:merchandise});
      }catch(err){
          res.status(500).json({error: err});
      }
};

const index = async(req, res) =>{
    try{
        const merchandise = await Merchandise.findAll();
        return res.status(200).json({merchandise});
    } catch(err){
        return res.status(500).json({err});
    }
};

const show = async(req,res) => {
    const {id} = req.params;
    try {
        const merchandise = await Merchandise.findByPk(id);
        return res.status(200).json({merchandise});
    }catch(err){
        return res.status(500).json({err});
    }
};

const update = async(req,res) => {
    const {id} = req.params;
    try {
        const [updated] = await Merchandise.update(req.body, {where: {id: id}});
        if(updated) {
            const merchandise = await Merchandise.findByPk(id);
            return res.status(200).send(merchandise);
        } 
        throw new Error();
    }catch(err){
        return res.status(500).json("Mercadoria não encontrada");
    }
};

const destroy = async(req,res) => {
    const {id} = req.params;
    try {
        const deleted = await Merchandise.destroy({where: {id: id}});
        if(deleted) {
            return res.status(200).json("Mercadoria deletada com sucesso.");
        }
        throw new Error ();
    }catch(err){
        return res.status(500).json("Mercadoria não encontrada.");
    }
};

const purchase = async(req,res) => {
    const{MerchandiseId, ClientId} = req.params;
    try {
        const client = await Client.findByPk(ClientId);
        const merchandise = await Merchandise.findByPk(MerchandiseId);
        await client.setMerchandise(merchandise);
        return res.status(200).json({msg: "Compra concluída!"});        
    }catch(err){
        return res.status(500).json({err});
    }
}

const cancelPurchase = async(req,res) => {
    const{MerchandiseId} = req.params;
    try {
        const merchandise = await Merchandise.findByPk(MerchandiseId);
        await merchandise.setClient(null);
        return res.status(200).json({msg: "Compra cancelada!"});        
    }catch(err){
        return res.status(500).json({err});
    }
}

const sell = async(req,res) => {
    const{MerchandiseId, SelerId} = req.params;
    try {
        const seller = await Seller.findByPk(SelerId);
        const merchandise = await Merchandise.findByPk(MerchandiseId);
        await seller.setMerchandise(merchandise);
        return res.status(200).json({msg: "Mercadoria colocado a venda!"});        
    }catch(err){
        return res.status(500).json({err});
    }
}

const cancelSell = async(req,res) => {
    const{MerchandiseId} = req.params;
    try {
        const merchandise = await Merchandise.findByPk(MerchandiseId);
        await merchandise.setSeller(null);
        return res.status(200).json({msg: "Anúncio da mercadoria removido!"});        
    }catch(err){
        return res.status(500).json({err});
    }
}

module.exports = {
    create,
    index,
    show,
    update,
    destroy,
    purchase,
    cancelPurchase,
    sell,
    cancelSell
}